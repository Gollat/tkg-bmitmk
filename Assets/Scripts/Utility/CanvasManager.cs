﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{

    #region fields
    [SerializeField] private Canvas _StartMenu;
    [SerializeField] private Canvas _NexLevelMenu;
    [SerializeField] private Canvas _WinMenu;
    [SerializeField] private Canvas _LoseMenu;
    [SerializeField] private Canvas _HUD;
    [SerializeField] private LevelManagement _LevelManager;
    [SerializeField] private UnityEngine.UI.Image _HpBar;
    [SerializeField] private Text _Spell;
    [SerializeField] private Text _Monsters;

    private bool _MenuEnabled;

    #endregion

    #region canvas actions
    public void LoadStartMenu(bool isEnabled)
	{
		if(isEnabled) Cursor.lockState = CursorLockMode.None;
        if (isEnabled) GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<PlayerAnimationAndMovement>().LockPlayer();
        _StartMenu.gameObject.SetActive (isEnabled);
	}

	public void LoadNextLevelMenu(bool isEnabled)
	{
		if(isEnabled) Cursor.lockState = CursorLockMode.None;
        if (isEnabled) GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<PlayerAnimationAndMovement>().LockPlayer();
        _NexLevelMenu.gameObject.SetActive (isEnabled);
    }

	public void LoadWinMenu(bool isEnabled)
	{
		if(isEnabled) Cursor.lockState = CursorLockMode.None;
        if (isEnabled) GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<PlayerAnimationAndMovement>().LockPlayer();
        _WinMenu.gameObject.SetActive (isEnabled);
        _MenuEnabled = true;
    }

	public void LoadLoseMenu(bool isEnabled)
	{
		 Cursor.lockState = CursorLockMode.None;
         GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<PlayerAnimationAndMovement>().LockPlayer();
        _LoseMenu.gameObject.SetActive (isEnabled);
   
    }

	public void LoadHUD(bool isEnabled)
	{
		if(isEnabled) Cursor.lockState = CursorLockMode.Locked;
		_HUD.gameObject.SetActive (isEnabled);
	}
	#endregion
	#region button actions
	public void LoadLevel()
	{
		LoadNextLevelMenu (false);
		_LevelManager.LoadNextLevel ();
        Cursor.lockState = CursorLockMode.Locked;
	}
	public void StartGame()
	{

		LoadStartMenu (false);
        GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<PlayerAnimationAndMovement>().UnlockPlayer();
        LoadHUD (true);
        Cursor.lockState = CursorLockMode.Locked;
	}
	public void MainMenu()
	{
		LoadLoseMenu (false);
		LoadWinMenu (false);
        _LevelManager.LoadLevel ("level1");
	}
	public void ExitGame()
	{
		Application.Quit ();
	}
	#endregion
	#region other actions
	public void SetHp(float fill)
	{
		_HpBar.transform.localScale = new Vector3 (fill, _HpBar.transform.localScale.y, _HpBar.transform.localScale.z);
	}
	public void SetSpell(string spell)
	{
		_Spell.text = "Spell: " + spell;
	}
	public void SetMonsters(int monsters)
	{
		_Monsters.text = "Monsters to kill: " + monsters.ToString ();
	}

    #endregion

}
