﻿using UnityEngine;
using System.Collections;

//Funkcja odpowiedzialna za zwrócenie elementów z punktami życie potworów i bohatera w kierunku kamery
//Żródło: http://wiki.unity3d.com/index.php?title=CameraFacingBillboard
public class CameraFacingBillboard : MonoBehaviour {

	public Camera choosenCamera;

	// Use this for initialization
	void Start () {
		choosenCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (transform.position + 
			Quaternion.Euler (choosenCamera.transform.rotation.eulerAngles.x, choosenCamera.transform.rotation.eulerAngles.y, 0) * Vector3.forward);
	}
}
