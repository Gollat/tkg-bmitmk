﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelManagement : MonoBehaviour
{
    private static int _Scene = 1;
	private int _LastLevel = 3;
   	static  GameObject _Player;
	private static string _LevelName;
    // Use this for initialization
    [SerializeField]
    private int _MurdersToNextLevel = 10;
	[SerializeField]
	private CanvasManager _CanvasManager;
    void Start()
    {
		_LevelName = SceneManager.GetActiveScene().name;
        _Player = GameObject.FindGameObjectWithTag("MainPlayer");
		_Scene = int.Parse (_LevelName.Replace ("level", ""));
		if (_Scene > 1)
			_CanvasManager.LoadHUD (true);
    }
    private void Update()
    {
		_CanvasManager.SetMonsters (_MurdersToNextLevel - _Player.gameObject.GetComponent<PlayerController> ().GetKilledEnemies ());
        if (ReadyToNextLevel())
        {
			Debug.Log (ReadyToNextLevel ());
			if (_Scene == _LastLevel) 
			{
				Debug.Log ("win ?");
				_CanvasManager.LoadHUD (false);
				_CanvasManager.LoadWinMenu (true);
			} 
				else 
			{
				Debug.Log ("next level");
				_CanvasManager.LoadHUD (false);
				_CanvasManager.LoadNextLevelMenu (true);
			}
        }
    }
    public  void LoadNextScene()
    {
		ResetPlayer ();
        _Scene++;
        string _SceneName = "level" + ((_Scene).ToString());
        SceneManager.LoadScene(_SceneName, LoadSceneMode.Single);
    }
    public  void LoadNextLevel()
    {
        LoadNextScene ();
    }

	public  void LoadLevel(string LevelName)
	{
     
		SceneManager.LoadScene(LevelName, LoadSceneMode.Single);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == _Player)
        {
            Debug.Log("FinishGame");
        }
    }
    public void ResetPlayer()
    {
        _Player.gameObject.GetComponent<PlayerController>().ResetMonstersInDangerZoneSet();
    }
    public  bool ReadyToNextLevel()
    {
        return (_Player.gameObject.GetComponent<PlayerController>().GetKilledEnemies() >= _MurdersToNextLevel) ? true : false;
    }
    public static string GetLevelName()
    {
        return _LevelName;
    }
    
}
