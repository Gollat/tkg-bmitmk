using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{

    // Use this for initialization

    public enum AnimationState { RUN, IDLE, ATTACK, HIT, DIE };
    private AnimationState _AnimationState;
    private Animator _Animator;
	[SerializeField] private AudioClip _SpiderAttack;
	[SerializeField] private AudioClip _SpiderHit;
	[SerializeField] private AudioClip _SpiderDie;
	[SerializeField] private AudioClip _RhinoAttack;
	[SerializeField] private AudioClip _RhinoHit;
	[SerializeField] private AudioClip _RhinoDie;
	[SerializeField] private AudioClip _SkeletonAttack;
	[SerializeField] private AudioClip _SkeletonHit;
	[SerializeField] private AudioClip _SkeletonDie;
	private AudioSource _AudioSource;

    void Start()
    {
        _AnimationState = AnimationState.IDLE;
        _Animator = GetComponent<Animator>();
		_AudioSource = GetComponent<AudioSource> ();
    }
    public void SetAnimationState()
    {
        MonsterController.MonsterState _State = this.gameObject.GetComponent<MonsterController>().GetMonsterState();
        SetAnimationState(_State);
    }

	public void PlayAudio(string AudioName)
	{
		Debug.Log ("sound");
		switch (AudioName) 
		{
			case "SpiderAttack":
				_AudioSource.clip = _SpiderAttack;
				break;
			case "SpiderHit":
				_AudioSource.clip = _SpiderHit;
				break;
			case "SpiderDie":
				_AudioSource.clip = _SpiderDie;
				break;
			case "RhinoAttack":
				_AudioSource.clip = _RhinoAttack;
				break;
			case "RhinoHit":
				_AudioSource.clip = _RhinoHit;
				break;
			case "RhinoDie":
				_AudioSource.clip = _RhinoDie;
				break;
			case "SkeletonAttack":
				_AudioSource.clip = _SkeletonAttack;
				break;
			case "SkeletonHit":
				_AudioSource.clip = _SkeletonHit;
				break;
			case "SkeletonDie":
				_AudioSource.clip = _SkeletonDie;
				break;
		}
		_AudioSource.Play ();
	}

    public void SetAnimationState(MonsterController.MonsterState _State)
    {
        if (_State == MonsterController.MonsterState.IDLE) _AnimationState = AnimationState.IDLE;
        if (_State == MonsterController.MonsterState.RUN) _AnimationState = AnimationState.RUN;
        if (_State == MonsterController.MonsterState.ATTACK) _AnimationState = AnimationState.ATTACK;
        if (_State == MonsterController.MonsterState.DIE) _AnimationState = AnimationState.DIE;
        if (_State == MonsterController.MonsterState.HIT) _AnimationState = AnimationState.HIT;
        Debug.Log("animation " + _AnimationState);
    }
    // Update is called once per frame
    void Update()
    {
        
        switch (_AnimationState)
        {
            case AnimationState.IDLE:
                {

                    Debug.Log("Idle");
                    _Animator.SetBool("run", false);
                    _Animator.SetBool("hit", false);

                    break;
                }
            case AnimationState.RUN:
                {

                    Debug.Log("Run");
                    _Animator.SetBool("run", true);
                    _Animator.SetBool("attack", false);
                    _Animator.SetBool("hit", false);

                    break;

                }
            case AnimationState.ATTACK:
                {

                    Debug.Log("Attack");
                    _Animator.SetBool("attack", true);
                    _Animator.SetBool("hit", false);

                    break;
                }
            case AnimationState.HIT:
                {

                    Debug.Log("HIT");
                    _Animator.SetBool("hit", true);

                    break;
                }
            case AnimationState.DIE:
                {

                    Debug.Log("dead");
                    _Animator.SetBool("dead", true);

                    break;
                }



        }
    }


}
