﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour {

    
	private BoxCollider _Territory;
	private GameObject _Player;

	private bool _PlayerInTerritory;

	private GameObject _Enemy;
	MonsterMovement _EnemyController;

    // Use this for initialization
    void Start()
    {    
        _Player = GameObject.FindGameObjectWithTag("MainPlayer");
		_Enemy = this.gameObject;
        _EnemyController = _Enemy.GetComponent<MonsterMovement>();
        _PlayerInTerritory = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_PlayerInTerritory)
        {
            _EnemyController.MoveToPlayer();
        }
        else 
        {
            _EnemyController.Rest();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == _Player)
        {
            _PlayerInTerritory = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == _Player)
        {
            _PlayerInTerritory = false;
        }
    }
    
}
