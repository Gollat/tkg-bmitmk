﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement : MonoBehaviour {

	private Transform _Target;
	private float _Speed = 3f;
	private float _AttackRange = 6f;
    //public int attack1Damage = 1;
    //public float timeBetweenAttacks;
	private Animation _Anim;
	private Animator _Animator;

	private string _CurrentAnimation;
    void Start()
    {
        _Target = GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<Transform>();

        Rest();
        _Anim = GetComponent<Animation>();
		if (_Anim == null) {
			_Animator = GetComponent<Animator> ();
		}
    }
    void Update()
    {
		if (_Anim != null) {
			if (!_Anim.isPlaying) {
				_Anim.Play (_CurrentAnimation);
			}
		}
    }

    public void MoveToPlayer()
    {

        //rotate to look at player
        transform.LookAt(_Target.position);
        transform.Rotate(new Vector3(0, 0, 0), Space.Self);

        //move towards player
        if (Vector3.Distance(transform.position, _Target.position) > _AttackRange)
        {
          //  _CurrentAnimation = "Run";
		//	if(_Animator != null) _Animator.SetBool ("run", true);
			transform.Translate(new Vector3(0, 0, _Speed * Time.deltaTime));
        }
        else {
            Attack();
        }
    }

    public void Attack() {
      //  _CurrentAnimation = "Attack";
		//if(_Animator != null) _Animator.SetTrigger ("attack");
    }

    public void Rest()
    {
       // _CurrentAnimation = "Idle";
		//if(_Animator != null) _Animator.SetBool ("run", false);
    }
    
}
