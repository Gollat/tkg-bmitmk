﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MonoBehaviour
{
    #region fields
    public enum MonsterState { RUN, IDLE, ATTACK, DIE, HIT };
    public enum MonsterType { RHINO, SPIDER, SKELETON };

    MonsterState _State;
    MonsterType _Type;

    [SerializeField] private float _AttackRange = 5.0f;
    [SerializeField] private float _HealthTotal = 100;
    [SerializeField] private float _Speed = 1.0f;
    [SerializeField] private float _AttackTimeRate = 1.0f;

    private bool _PlayerInTerritory;
    private float _DistanceToPlayer;
    private float _CollisionWithPlayer = 20.0f;
    private float _NextAttackTime = 0.0f;
    private float _RecoverTimeRate = 1.2f; //1.2f
    private float _RecoverTime = 0.0f;
    private float _AttackTime = 2.0f;
    private float _Hit = 0.0f;
    private float _Hp;
    private BoxCollider _Territory;
    private GameObject _Player;
    private GameObject _Enemy;
    private MonsterController _EnemyController;
    private AnimationState _AnimationState;
    private AnimationController _AnimationController;
    private Transform _Target;
    private Animation _Animation;
	private UnityEngine.UI.Image _HpBar;
    #endregion
    void Start()
    {
        _State = MonsterState.IDLE;
        _Target = GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<Transform>();
        _Player = GameObject.FindGameObjectWithTag("MainPlayer");
		_Enemy = this.gameObject;
        _EnemyController = _Enemy.GetComponent<MonsterController>();
        _PlayerInTerritory = false;
        _Hp = _HealthTotal;
        _AnimationController = _Enemy.GetComponent<AnimationController>();
		UnityEngine.UI.Image[] images = GetComponentInChildren<Canvas> ().GetComponentsInChildren<UnityEngine.UI.Image> ();
        SetMonsterType();
		foreach (UnityEngine.UI.Image img in images) 
		{
			if (img.name == "HP Bar") 
			{
				_HpBar = img;
			}
		}
        SetMonsterType();
  
    }

    void Update()
    {
        LookAtPlayer();
        CheckIfInDangerZone();
        Action();
    }

    public void Action()
    {
       // Idle();
    
        if (PlayerInTerritory() && !PlayerInAttackRange() && Time.time >_NextAttackTime&& ! IsRecovering())
        {
            RunToPlayer();
            return;
        }
        if (IsRecovering() && PlayerInTerritory())
        {
            SetState(MonsterState.HIT);
            return;
        }
        if (IsAttacking())
        {
            Attack();
            return;
        }
   
        if (_Hit != 0 )
        {
            GotHit();
            return;
        }
    }

    #region conditions functions
    public bool IsDead()
    {
        return _Hp <= 0 ? true : false;
    }
    public bool IsAttacking()
    {
        return (PlayerInAttackRange() && Time.time > _NextAttackTime && !IsDead()) ? true : false;
    }
    public bool PlayerInAttackRange()
    {
        return _DistanceToPlayer <= _AttackRange ? true : false;
    }
    public bool IsRecovering()
    {
        return (Time.time <= _RecoverTime) ? true : false;
    }
    public bool IsRunning()
    {
        return (_DistanceToPlayer < _CollisionWithPlayer && _DistanceToPlayer > _AttackRange && !IsRecovering() && !IsAttacking()) ? true : false;
    }
    public bool PlayerInTerritory()
    {
        return _PlayerInTerritory;
    }
    public void CheckIfInDangerZone()
    {
        _DistanceToPlayer = Vector3.Distance(transform.position, _Target.position);
        if (_DistanceToPlayer < _Player.GetComponent<PlayerController>().GetAttackRange())
        {
            PlayerController.AddToMonstersInDangerSet(_EnemyController);
        }
    }
    #endregion

    #region interaction 
    public MonsterState GetMonsterState()
    {
        return _State;
    }
    public float GetHealth()
    {
        return _Hp;
    }
    public float GetHealthTotal()
    {
        return _HealthTotal;
    }
    public void Hit(float val)
    {
        _Hit = val;
    }
    #endregion

    #region actionfunctions
    private IEnumerator Destroy()
    {
        SetState(MonsterState.DIE);
        yield return new WaitForSeconds(10);
        PlayerController.RemoveFromMonstersInDangerZoneSet(this.GetComponent<MonsterController>());
		_Player.gameObject.GetComponent<PlayerController>().IncreaseKilledEnemies();
		Debug.Log (_Player.gameObject.GetComponent<PlayerController> ().GetKilledEnemies ());
        Destroy(_Enemy);

    }
    private void Attack()
    {
        SetNextAttackTime();
        float _AttackStrength =SetAttackStrength();
        _Player.GetComponent<PlayerController>().DecreaseHealth(_DistanceToPlayer / 10*_AttackStrength);
        _AnimationController.SetAnimationState(MonsterState.ATTACK);
        _State = MonsterState.ATTACK;
    }
    private void GotHit()
    {
        if (_Hit != 0)
        {
            Debug.Log("get hit");
            SetNextAttackTime();

            _State = MonsterState.HIT;
            if (!IsRecovering())
            {
                if (_Type == MonsterType.RHINO) _Hit /= 2;
                if (_Type == MonsterType.SKELETON) _Hit /= 4;
                DecreaseHp(_Hit);
                _Hit = 0;
                if (IsDead())
                {
                    StartCoroutine(Destroy());
                }
                SetRecoverTime();
            }

        }
    }
    public void Idle()
    {
        _State = MonsterState.IDLE;
        _AnimationController.SetAnimationState(_State);
    }

    private void LookAtPlayer()
    {
        Vector3 Position = new Vector3(_Target.position.x, 1, _Target.position.z);
        transform.LookAt(Position);
        transform.Rotate(new Vector3(0, 0, 0), Space.Self);
    }
    private void RunToPlayer()
    {
		if (!IsDead ()) 
		{
			CheckIfInDangerZone ();
			LookAtPlayer ();
			transform.Translate (0, 0, _Speed * Time.deltaTime);
			_State = MonsterState.RUN;
			_AnimationController.SetAnimationState (_State);
		}
    }
    #endregion

    #region setters
    private void SetRecoverTime()
    {
        _RecoverTime = Time.time + _RecoverTimeRate;
    }
    private void DecreaseHp(float value)
    {
        _Hp -= value;
		if (_Hp < 0)
			_Hp = 0;
		_HpBar.transform.localScale = new Vector3 (_Hp / _HealthTotal, _HpBar.transform.localScale.y, _HpBar.transform.localScale.z);
    }

    public void SetNextAttackTime()
    {
        _NextAttackTime = Time.time + _AttackTimeRate;
    }
    public void SetState(MonsterController.MonsterState _MonsterState)
    {
        _State = _MonsterState;
        _AnimationController.SetAnimationState(_State);
    }
    public float SetAttackStrength()
    {
        return (_Type == MonsterType.SPIDER ? 1.3f : _Type == MonsterType.RHINO ? 1.5f : 3f);
    }
    public void SetMonsterType()
    {
        if (_Enemy.tag == "Rhino") _Type= MonsterType.RHINO;
        if (_Enemy.tag == "Skeleton") _Type= MonsterType.SKELETON;
        if (_Enemy.tag == "Spider")  _Type =MonsterType.SPIDER;
    }
    #endregion

    #region player in territory
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == _Player)
        {
            _PlayerInTerritory = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == _Player)
        {
            _PlayerInTerritory = false;
        }
    }

    #endregion
}