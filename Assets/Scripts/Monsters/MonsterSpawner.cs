﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MonsterSpawner : MonoBehaviour {

	[SerializeField]
	private GameObject _MonsterInstance;
	[SerializeField]
	private int _MonsterCount = 10;

	private float _BoundN = -100.0f;
	private float _BoundS = 250.0f;
	private float _BoundE = 200.0f;
	private float _BoundW = -200.0f;

	private float _PosX;
	private float _PosZ;
	private float _PosY;
	// Use this for initialization
	void Start () {
        string _SceneName = SceneManager.GetActiveScene().name;
		if (_SceneName == "level3") {
			_BoundN = 180.0f;
			_BoundS = 370.0f;
			_BoundW = 320.0f;
			_BoundE = 180.0f;
			Cursor.lockState = CursorLockMode.Locked;
		} else if (_SceneName == "level2") 
		{
			Cursor.lockState = CursorLockMode.Locked;
		}
		for (int i = 0; i < _MonsterCount; i++) {
			_PosX = Random.Range (_BoundN, _BoundS);
			_PosZ = Random.Range (_BoundW, _BoundE);
			Debug.Log (Terrain.activeTerrain.name);
			_PosY = Terrain.activeTerrain.SampleHeight (new Vector3 (_PosX, 0, _PosZ));
            if (_SceneName == "level3") _PosY = 1.2f;
			GameObject _NewEnemy = Instantiate (_MonsterInstance);
			_NewEnemy.transform.position = new Vector3 (_PosX, _PosY, _PosZ);
			_NewEnemy.transform.rotation = Quaternion.Euler (0, Random.Range (0, 360), 0);
		}
	}
}
