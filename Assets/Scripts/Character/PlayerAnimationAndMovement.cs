﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationAndMovement : MonoBehaviour
{

    [SerializeField]   private GameObject _FireSpell;
    [SerializeField]   private GameObject _IceSpellShards;
    [SerializeField]   private GameObject _IceSpellMist;
    [SerializeField]   private GameObject _LightSpellMaster;
    [SerializeField]   private GameObject _LightSpellOrange;
    [SerializeField]   private GameObject _LightSpellRainbow;
    [SerializeField]   private AudioClip _Ice;
    [SerializeField]   private AudioClip _Fire;
    private AudioSource _AudioSource;
    private string _CurrentSpell = "Fire";
    private CanvasManager _CanvasManager;
    private bool _LockPlayer = true;
    private Animator _Animator;
    bool _Spell = false;
    float _Speed;
    bool Collision = false;
    PlayerController _Player;
    LevelManagement _LevelManager;
    void Start()
    {
        _Animator = GetComponent<Animator>();
        _Speed = gameObject.GetComponent<PlayerController>().GetSpeed();
        _Player = GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<PlayerController>();
        _CanvasManager = _CanvasManager = GameObject.FindObjectOfType<CanvasManager>();
        _AudioSource = GetComponentInChildren<AudioSource>();
        _Speed = _Player.GetSpeed();
        _FireSpell.SetActive(false);
        _IceSpellMist.SetActive(false);
        _IceSpellShards.SetActive(false);
        _LightSpellOrange.SetActive(false);
        _LightSpellRainbow.SetActive(false);
        _LightSpellMaster.SetActive(false);
    }
    #region oncollision
    void OnCollisionEnter(Collision c)
    {
        if (!(c.gameObject.name == "Terrain"))
            Collision = true;
    }
    void OnCollisionExit(Collision c)
    {
        if (!(c.gameObject.name == "Terrain"))
        {
            Collision = false;
        }
    }
    #endregion

    #region spell coroutines
    private IEnumerator CastFire()
    {
        _FireSpell.SetActive(true);
        _AudioSource.clip = _Fire;
        _AudioSource.Play();
        yield return new WaitForSeconds(2);
        _FireSpell.SetActive(false);
    }

    private IEnumerator CastIce()
    {
        _IceSpellShards.SetActive(true);
        _IceSpellMist.SetActive(true);
        _AudioSource.clip = _Ice;
        _AudioSource.Play();
        yield return new WaitForSeconds(2);
        _IceSpellShards.SetActive(false);
        _IceSpellMist.SetActive(false);
    }
    private IEnumerator CastLight()
    {

        _LightSpellOrange.SetActive(true);
        _LightSpellRainbow.SetActive(true);
        _LightSpellMaster.SetActive(true);
        _AudioSource.clip = _Ice;
        _AudioSource.Play();
        yield return new WaitForSeconds(2);
        _LightSpellOrange.SetActive(false);
        _LightSpellRainbow.SetActive(false);
        _LightSpellMaster.SetActive(false);
    }
    #endregion

    void Update()
    {

        CheckLockContition();
        if (!(_LockPlayer))
        {
           // Cursor.lockState = CursorLockMode.Locked;
            if (Input.GetKeyDown(KeyCode.F1))
            {
                _Animator.SetFloat("spell", 0);
                _CurrentSpell = "Fire";
                _CanvasManager.SetSpell(_CurrentSpell);
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                _Animator.SetFloat("spell", 1);
                _CurrentSpell = "Ice";
                _CanvasManager.SetSpell(_CurrentSpell);
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                _Animator.SetFloat("spell", 2);
                _CurrentSpell = "Light";
                _CanvasManager.SetSpell(_CurrentSpell);
            }

            if (Input.GetMouseButtonDown(0))
            {
                _Spell = true;
                _Player.SetSpell(_Spell);
                _Animator.SetTrigger("casting");
                switch (_CurrentSpell)
                {
                    case "Fire":
                        StartCoroutine(CastFire());
                        break;
                    case "Ice":
                        StartCoroutine(CastIce());
                        break;
                    case "Light":
                        StartCoroutine(CastLight());
                        break;
                }
            }
            float translation = Input.GetAxis("Vertical") * _Speed;
            float straffe = Input.GetAxis("Horizontal") * _Speed;

            translation *= Time.deltaTime;
            straffe *= Time.deltaTime;

            if (!Collision)
            {

                transform.Translate(straffe, 0, translation);
            }
            else
            {
                transform.Translate(straffe, 0, -translation);
            }
            if ((translation != 0) || (straffe != 0))
            {
                _Animator.SetBool("walking", true);
            }
            else
            {
                _Animator.SetBool("walking", false);
            }

            if (translation > 0)
            {
                _Animator.SetFloat("forward", 1);
            }
            else
            {
                _Animator.SetFloat("forward", -1);
            }

            if (straffe > 0)
            {
                _Animator.SetFloat("right", 1);
            }
            else
            {
                _Animator.SetFloat("right", -1);
            }

            if (Input.GetKeyDown("escape"))
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }


    public void SetSpell(bool _S)
    {
        _Spell = _S;
    }
    public string GetCurrentSpell()
    {
        return _CurrentSpell;
    }
    public void LockPlayer()
    {
        _LockPlayer = true;
    }
    public void UnlockPlayer()
    {
        _LockPlayer = false;
    }

    public void CheckLockContition()
    {
        string _LevelName = LevelManagement.GetLevelName();
        if (_LevelName != "level1") UnlockPlayer();
    }
}
