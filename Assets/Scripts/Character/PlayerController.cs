﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	[SerializeField] private float _Health = 50.0f;
	[SerializeField] private float _PlayerAttackRange =10.0f;
	[SerializeField] private float _AttackRate = 100.0f;
	[SerializeField] private float _Speed = 10.0f;
	[SerializeField] private float _AttackRateTime = 3.0f;
	private float _Hp;
	private float _NextAttackTime = 0.0f;
	private bool Collision = false;
	private bool _Spell =false;
	private CanvasManager _CanvasManager;

   
    private int _KilledEnemies = 0;
	private static HashSet<MonsterController> MonstersInDanger = new HashSet<MonsterController>();
	static Transform _Player;
	void Start () {
		_Player= GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<Transform>();
		_CanvasManager = GameObject.FindObjectOfType<CanvasManager> ();
		_Hp = _Health;
	}
    void Update () {

		if (_Spell == true)
		{
			Attack();
			_Spell = false;
		}
		if (IsDead ()) 
		{
			_CanvasManager.LoadHUD (false);
			_CanvasManager.LoadLoseMenu (true);
		}
	}

    internal void ResetMonstersInDangerZoneSet()
    {
        MonstersInDanger.Clear();
    }

    #region fight
    public bool IsDead(){
		return (_Hp> 0) ? false : true;
	}
    internal void SetSpell(bool v)
    {
        _Spell = v;
    }
    public void Attack()
	{
		if (Time.time > _NextAttackTime)
		{
			
			foreach (MonsterController monster in MonstersInDanger)
			{
				Transform _Target = monster.GetComponent<Transform>();
				float DistanceToPlayer = Vector3.Distance(_Player.position, _Target.position);
				monster.Hit(_AttackRate*SetSpellStrength()/DistanceToPlayer);
			}
			_NextAttackTime = Time.time+ _AttackRateTime;
		}
	}
    public static void AddToMonstersInDangerSet(MonsterController _MonsterController)
    {
        MonstersInDanger.Add(_MonsterController);
    }
	public static void RemoveFromMonstersInDangerZoneSet(MonsterController MonsterController)
	{
		MonstersInDanger.Remove(MonsterController);
	}
	public void DecreaseHealth(float Damage)
	{
            _Hp -= Damage;
            Debug.Log("Player " + _Hp);
			if (_Hp < 0)
				_Hp = 0;
		_CanvasManager.SetHp (_Hp / _Health);
	}
    public float GetAttackRange()
    {
        return _PlayerAttackRange;
    }
    #endregion

    public void IncreaseKilledEnemies()
    {
        _KilledEnemies++;
    }
    public int GetKilledEnemies()
    {
        return _KilledEnemies;
    }
    public float GetSpeed()
    {
        return _Speed;
    }
    private float SetSpellStrength()
    {
        string Spell = _Player.GetComponent<PlayerAnimationAndMovement>().GetCurrentSpell();
        return Spell == "ice" ? 1 : Spell == "fire " ? 2 : 3; 
        
    }
  
}