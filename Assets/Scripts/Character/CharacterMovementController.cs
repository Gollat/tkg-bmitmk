﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour {

    [SerializeField]
	private float _Speed = 10.0f;
	private Animator _Anim;
	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.Locked;
		_Anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.F1)) {
			_Anim.SetFloat ("spell", 0);
		} else if (Input.GetKeyDown (KeyCode.F2)) {
			_Anim.SetFloat ("spell", 1);
		} else if (Input.GetKeyDown (KeyCode.F3)) {
			_Anim.SetFloat ("spell", 2);
		}

		if (Input.GetMouseButtonDown (0)) {
			_Anim.SetTrigger ("casting");
		}
		float _Translation = Input.GetAxis("Vertical") * _Speed;
		float _Straffe = Input.GetAxis("Horizontal") * _Speed;

        _Translation *= Time.deltaTime;
        _Straffe *= Time.deltaTime;

        transform.Translate(_Straffe, 0, _Translation);
		if ((_Translation != 0) || (_Straffe != 0)) {
			_Anim.SetBool ("walking", true);
		} else {
			_Anim.SetBool ("walking", false);
		}

		if (_Translation > 0) {
			_Anim.SetFloat ("forward", 1);
		} else {
			_Anim.SetFloat ("forward", -1);
		}

		if (_Straffe > 0) {
			_Anim.SetFloat ("right", 1);
		} else {
			_Anim.SetFloat ("right", -1);
		}

        if (Input.GetKeyDown("escape")) {
            Cursor.lockState = CursorLockMode.None;
        }
	}
}
